package com.oreillyauto.dao;

import org.springframework.data.repository.CrudRepository;

import com.oreillyauto.dao.custom.ProjectRepositoryCustom;
import com.oreillyauto.domain.projects.Project;

public interface ProjectRepository extends CrudRepository<Project, Integer>, ProjectRepositoryCustom {
    
}
