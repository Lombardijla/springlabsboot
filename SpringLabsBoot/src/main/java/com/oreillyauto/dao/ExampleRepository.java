package com.oreillyauto.dao;

import org.springframework.data.repository.CrudRepository;

import com.oreillyauto.dao.custom.ExampleRepositoryCustom;
import com.oreillyauto.domain.examples.Example;

/*
 *  CrudRepository<Example, Integer>  <== IMPORTANT: The integer is the datatype of your GUID (PK) on your table  
 */ 
public interface ExampleRepository extends CrudRepository<Example, Integer>, ExampleRepositoryCustom {
    
    // Add your interface abstract methods here (that ARE Spring Data methods)
    
}
