package com.oreillyauto.service;

import java.util.List;

import com.oreillyauto.domain.examples.UserRole;

public interface UserRolesService {

	public List<UserRole> getUserRoles();
	
}
