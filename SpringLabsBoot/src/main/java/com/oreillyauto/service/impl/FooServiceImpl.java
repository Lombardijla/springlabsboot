package com.oreillyauto.service.impl;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oreillyauto.dao.ClassRepository;
import com.oreillyauto.dao.PupilRepository;
import com.oreillyauto.domain.foo.Clazz;
import com.oreillyauto.domain.foo.Pupil;
import com.oreillyauto.service.FooService;

@Service("fooService")
public class FooServiceImpl implements FooService {

	@Autowired
	PupilRepository pupilRepo;
	@Autowired
	ClassRepository classRepo;
	
	public FooServiceImpl() {
	}

	@Override
	public List<Pupil> getPupils() {
		return (List<Pupil>)pupilRepo.findAll();
	}

	@Override
	public List<Clazz> getClasses() {
		return (List<Clazz>)classRepo.findAll();
	}

	@Override
	public Pupil addPupil(Pupil pupil) {
		return pupilRepo.save(pupil);
	}

	@Override
	public Map<String,Clazz> getClassesQdsl() {
		return classRepo.getClasses();
	}

	@Override
	public Integer getTransactionCount(Map<String, Clazz> clazzMap) {
		Integer txCount = 0;
		
		for (Map.Entry<String, Clazz> entry : clazzMap.entrySet()) {
		    Clazz clazz = (Clazz)entry.getValue();
		    List<Pupil> pupilList = clazz.getPupilList();
		    
		    if (pupilList != null) {
		    	txCount += pupilList.size();
		    }
		}
		
		return txCount;
	}

}
