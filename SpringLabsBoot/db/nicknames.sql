DROP TABLE IF EXISTS nicknames;

CREATE TABLE nicknames (
  id INTEGER AUTO_INCREMENT,
  nick_name VARCHAR(32),
  PRIMARY KEY(id)
);

INSERT INTO nicknames (nick_name) 
VALUES
 ('giraffe')
,('dawg')
,('giggles');