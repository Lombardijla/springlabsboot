DROP TABLE IF EXISTS university;
DROP TABLE IF EXISTS course;

CREATE TABLE course (
       course_guid INTEGER NOT NULL AUTO_INCREMENT,
       course_name VARCHAR(100),
       credit INTEGER,
	   professor_id INTEGER,
	   room_id INTEGER,
       CONSTRAINT PK_course_id PRIMARY KEY(course_guid)
);

CREATE TABLE university (
	university_guid INTEGER NOT NULL AUTO_INCREMENT,
    university_name VARCHAR(32),
    course_id INTEGER,
    CONSTRAINT PK_university_guid PRIMARY KEY(university_guid)
);

ALTER TABLE university
ADD CONSTRAINT FK_course_id_course_guid
FOREIGN KEY(course_id) REFERENCES course(course_guid);

--INSERT INTO course (course_name, credit, professor_id, room_id) 
--VALUES 
-- ('Geometry', 3, 1, 2)
--,('Calculus', 3, 1, 1)
--,('Trigonometry', 4, 1, 3)
--,('Physics', 4, 2, 4)
--,('Biology', 6, 3, 5)
--,('Chemistry', 6, 4, 5)
--,('Philosophy', 3, 6, 2)
--,('Ethics', 3, 5, 1)
--,('Micro Economics', 3, 5, 2);
--
--INSERT INTO university (university_name, course_id)
--VALUES 
-- ('MSU', 1)
--,('MSU', 2)
--,('MSU', 4)
--,('MSU', 5)
--,('MSU', 6)
--,('MSU', 7)
--,('MSU', 9)
--,('WKU', 1)
--,('WKU', 2)
--,('WKU', 3)
--,('WKU', 4)
--,('WKU', 5)
--,('WKU', 6)
--,('WKU', 7)
--,('WKU', 8)
--,('WKU', 9);
